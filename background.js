(function() {
    var query_hash = '0f318e8cfff9cc9ef09f88479ff571fb';
    var variables = '{"id":"9168203377"}';

    function SetBadge(n) {
        chrome.browserAction.setBadgeBackgroundColor({ color: [255, 0, 0, 255] });
        chrome.browserAction.setBadgeText({text: n.toString()});
        localStorage.setItem('badge', n);
    }

    function SetupProxy() {
        chrome.webRequest.onBeforeSendHeaders.addListener(function (data) {
            data.requestHeaders.push({
                name: 'Origin',
                value: 'https://www.instagram.com/'
            });
            data.requestHeaders.push({
                name: 'referer',
                value: 'https://www.instagram.com/'
            });
            return {
                requestHeaders: data.requestHeaders
            };
        }, {
            urls: ['https://*.instagram.com/*']
        }, ["requestHeaders", "blocking"]);

        chrome.webRequest.onHeadersReceived.addListener(function(info) {
            var headers = info.responseHeaders;
            for (var i = headers.length - 1; i >= 0; --i) {
                var header = headers[i].name.toLowerCase();
                if (header === 'x-frame-options' || header === 'frame-options') {
                    headers.splice(i, 1); // Remove header
                }
            }
            return {
                responseHeaders: headers
            };
        }, {
            urls: ['https://*.instagram.com/*']
        }, ['blocking', 'responseHeaders']);
    }

    function SetupWatcher() {function watch() {
        $.get('https://www.instagram.com/accounts/activity/?__a=1', function(data) {
            var activity = data.graphql.user.activity_feed.edge_web_activity_feed.edges;
            var newtimestamp = data.graphql.user.activity_feed.timestamp;
            var oldtimestamp = localStorage.getItem('timestamp') || newtimestamp;
            var oldbadge = parseInt(localStorage.getItem('badge') || 0);
            activity = activity.filter(function (x) {
                return x.node.timestamp > oldtimestamp;
            });
            SetBadge(oldbadge + activity.length);
            localStorage.setItem('timestamp', newtimestamp);
        });
        $.get('https://www.instagram.com/graphql/query/?query_hash='+query_hash+'&variables='+variables+'&chrome-extension=true', function(data) {
            if (typeof data.data === 'object' && data.data != null && data.data.user != null && data.data.user.edge_activity_count != null) {
                var activity = data.data.user.edge_activity_count.edges;
                var count = 0;
                for (var i = 0; i < activity.length; i++) {
                    count += activity[i].node.comment_likes + activity[i].node.comments + activity[i].node.likes + activity[i].node.relationships + activity[i].node.usertags
                }
                if (count < 40) {
                    // maximum activity = 40. If < 40 it's reset
                    SetBadge(count);
                }
            }
        });
    } setInterval(watch, 10000);}

    SetupProxy();
    SetupWatcher();
})();
