(function(){
    function onPopup() {
        try {
            return window.self !== window.top;
        } catch (e) {
            return true;
        }
    }
    if (!onPopup()) {
        return;
    }

    Object.defineProperty(navigator, 'userAgent', {
        value: 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Mobile Safari/537.36',
        configurable: true // necessary to change value more than once
    });
    Object.defineProperty(navigator, 'appVersion', {
        value: '5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Mobile Safari/537.36',
        configurable: true // necessary to change value more than once
    });
    Object.defineProperty(navigator, 'maxTouchPoints', {
        value: 1,
        configurable: true // necessary to change value more than once
    });

    var actualCode =  '(' + function() {
        'use strict';
        var modifiedNavigator;
        var modifiedScreenOrientation;

        if ('userAgent' in Navigator.prototype)
            modifiedNavigator = Navigator.prototype;

        if ('type' in ScreenOrientation.prototype)
            modifiedScreenOrientation = ScreenOrientation.prototype;

        Object.defineProperties(modifiedNavigator, {
            userAgent: {
                value: 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Mobile Safari/537.36',
                configurable: false,
                enumerable: true,
                writable: false
            },
            appVersion: {
                value: '5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Mobile Safari/537.36',
                configurable: false,
                enumerable: true,
                writable: false
            },
            maxTouchPoints: {
                value: 1,
                configurable: false,
                enumerable: true,
                writable: false
            },
        });

        Object.defineProperties(modifiedScreenOrientation,{
            type: {
                value: 'portrait-primary',
                configurable: false,
                enumerable: true,
                writable: false
            },
        });
    } + ')();';

    var s = document.createElement('script');
    s.textContent = actualCode;
    document.documentElement.appendChild(s);
    s.remove();
})();
